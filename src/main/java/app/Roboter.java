package app;

public class Roboter {
  private enum Status { WALKING, JUMPING, SHOOTING }
  public Roboter() {
    App.main(new String[0]);
  }
  
  /**
   * Says hello
   */
  public void sayHello() {
    System.out.println("hello"); 
  }
  
  /**
   * Shoots towards direction
   * @param direction "u","d","l" or "r"
   * @return new Status of robot
   */
  public Status shoot(String direction) {
    return Status.SHOOTING;
  }
}
